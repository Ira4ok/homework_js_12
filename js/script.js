/*Regular way step-by-step function development */
// const slideImgs = document.querySelectorAll('.image-to-show');
// const stopButton = document.querySelector('#stop-btn');
// const continueButton = document.querySelector('#continue-btn');
//
// let counter = 0;
// let check = true;
//
// const showSlides = () => {
//     slideImgs[counter].classList.add('image-hide');
//     counter += 1;
//     if (slideImgs.length === counter) {
//         counter = 0;
//     }
//     slideImgs[counter].classList.remove('image-hide');
// };
//
// let slidesInterval = setInterval(showSlides, 1000);
//
// stopButton.addEventListener('click', () => {
//    clearInterval(slidesInterval);
//    check = false;
//
// });
//
// continueButton.addEventListener('click', () => {
//     if (check === false) {
//         slidesInterval = setInterval(showSlides, 1000);
//         check  = true
//     }
// });
//


/*Function-constructor*/
function ImageShow(counter, imageClass) {
    this.slideImgs = document.querySelectorAll(`.${imageClass}`);
    let check = true;

    this.createButtons = function () {
        let stopButton = document.createElement('button');
        stopButton.classList.add('stop-btn');
        stopButton.innerText = 'Stop';
        let continueButton = document.createElement('button');
        continueButton.classList.add('continue-btn');
        continueButton.innerText = 'Continue';

        stopButton.addEventListener('click', () => {
            clearInterval(this.startInterval);
            check = false;
        });

        continueButton.addEventListener('click', () => {
            if (check === false) {
                this.showSlides();
                check = true
            }
        });
        document.body.appendChild(stopButton);
        document.body.appendChild(continueButton);
    };
        this.showSlides = function () {
            this.startInterval = setInterval(() => {
                this.slideImgs[counter].classList.add('image-hide');
                counter += 1;

                if (this.slideImgs.length === counter) {
                    counter = 0;
                }
                this.slideImgs[counter].classList.remove('image-hide');
            }, 1000);
        };
}
let imgShow = new ImageShow(0, 'image-to-show');
imgShow.createButtons();
imgShow.showSlides();

// let imgShow2 = new ImageShow(0, 'image-to-show');
// imgShow2.createButtons();
// imgShow2.showSlides();